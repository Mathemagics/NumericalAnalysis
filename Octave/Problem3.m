L=10
r=2
V=40
f=@(L,h,r,V)(L*(0.5*pi*(r**2)-(r**2)*asin(h/r)-h*sqrt((r**2)-(h**2)))-V)

%The depth is the radius minus the distance from the top, h.
%So, we must first find h using FPI, then subtract it from r
depth=r-fzero(@(h)f(L,h,r,V),[0,r])
%In the previous we guessed that the zero lies 
%Between 0 and 1, 
%Below zero doesn't make sense.
%More than the radius=1, would mean that the trough is overflowing


%Checking Answer:
%If we subtract the depth from r, we get h.
%If we substitute this expression into our FPI, we should get 0.
%For the following, we should get boolean=1 true
round(f(L,r-depth,r,V))==0

R=3/2
V=7/3*pi
f=@(H,R,V)(pi*R*H.^2-(1/3)*pi*H.^3-V)
depth=fzero(@(H)f(H,R,V),[0,2*R])
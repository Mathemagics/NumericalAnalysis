graphics_toolkit('gnuplot')
%Finding Minimums
Cp= inline( '(96000*(64+(16-x)^2)^2*(16-x))/((64+(16-x)^2)^3)^(3/2)-(36000*x*(36+x^2)^2)/((36+x^2)^3)^(3/2)');
%fplot(Cp, [0,50])

min1=fminbnd(Cp,0,5)
min2=fminbnd(Cp,15,20)

%Finding Maximum
CpNegative= inline( '-1*((96000*(64+(16-x)^2)^2*(16-x))/((64+(16-x)^2)^3)^(3/2)-(36000*x*(36+x^2)^2)/((36+x^2)^3)^(3/2))');
maximum=fminbnd(CpNegative,10,15);




%TESTING

%Finding Equally Illuminated
Cp= inline( '(96000*(64+(16-x)^2)^2*(16-x))/((64+(16-x)^2)^3)^(3/2)-(36000*x*(36+x^2)^2)/((36+x^2)^3)^(3/2)');
%fplot(Cp, [0,50])

joined=inline('(2000*6)/(sqrt((6.^2+x.^2).^3))+(4000*8)/(sqrt((8.^2+(16-x).^2).^3))')
lamp1=inline('(2000*6)/(sqrt((6.^2+x.^2).^3))')
lamp2=inline('(4000*8)/(sqrt((8.^2+(16-x).^2).^3))')
%fplot(lamp1,[0,50])
hold off
%fplot(joined,[0,50])
%test2=fzero(test,[5,7])

fplot(Cp,[0,50])
fzero(Cp,[5,10])
fzero(Cp,[14,17])
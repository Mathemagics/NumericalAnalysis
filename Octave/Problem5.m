pkg load odepkg
graphics_toolkit('gnuplot')
%Constants
GM=1;
%Initial Conditions
x0=.5;
vx0=0;
y0=0;
vy0=1.15;

vy0=2+.00000
z=[x0,vx0,y0,vy0];
tspan = [0,900000000*2*pi];
[t,f] = ode45(@p5,tspan,z);

x=f(:,1);
vz=f(:,2);
y=f(:,3);
vy=f(:,4);

plot(x,y)



r=sqrt(x.^2+y.^2);

semiMajorAxis = max(max(abs(x)),max(abs(y)))
period=2*pi*sqrt(semiMajorAxis^3)
Vescape=sqrt(2*GM/semiMajorAxis)
VCircular=sqrt(GM/sqrt(x0)^2)

%semiMajorAxis =  0.50000;
%semiMinorAxis= max(x);
%period =  2.2214;
%Vescape =  2;

h=0.06;
x=0:h:1;
clear y;
y(1)=2;
f=inline("2*y+x")

for i=1:16 y(i+1)=y(i)+h*f(x(i),y(i)); end
Y_exact=(1/4)*(-1+9*exp(2*x)-2*x);

plot(x,y,"--",x,Y_exact)
title("Solution to y'=2y+x")
legend("Euler Method with h=0.06", "Exact Solution")

for i=1:17 y_e(i)=Y_exact(i); end
absoluteDiff=abs(y-y_e)
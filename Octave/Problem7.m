pkg load odepkg
%Initial Conditions
global g alpha w etha x

etha=0;

h=1.2;
theta=(18/180)*pi;
v0=26;

vx0=v0*cos(theta);
x0=0;

z0=h;
vz0=v0*sin(theta);

%Sys ODE Solving
x=[x0,z0,vx0,vz0];
tspan = [0,3];
[t,f] = ode45(@p7,tspan,x);

%Convenience bindings
x=f(:,1);
z=f(:,2);
vx=f(:,3);
vz=f(:,4);

%Plotting
plot(x,z)
hold on
%plot(t,x,t,z)
legend("Trajectory")
xlabel('X-Displacment')
ylabel('Z-Displacement')

%Interpolate trajectory over finer abscicssa, using Spline
xnew=[0:.1:30];
znew=@(xnew)spline(x,z,xnew);
%plot(xnew,znew(xnew))
%legend("Interpolated Trajectory")
%xlabel('X-Displacment')
%ylabel('Z-Displacement')

MaxRange=fzero(znew,[20,24])

%Now need to find out what [t,x] gives maxRange
tnew=[0:.1:3];
f=@(tnew)MaxRange-spline(t,x,tnew);
FlightTime=fzero(f,[0,3])

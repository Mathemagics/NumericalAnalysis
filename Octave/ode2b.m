function dzdt = ode2b(t,z);
% ODE2B: computes dzdt.
g = 9.81; k = 1; m = 1;
dzdt = [z(2); g-(k/m)*sqrt(abs(z(2)))];

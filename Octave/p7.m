function xdot= p7(t,x)

global g alpha w etha

w=27;
g=9.81;
rho=1.28;
d=.063;
m=.05;
alpha=(rho*pi*d.^2)/(8*m);

v=sqrt(x(3)^2 + x(4)^2);
Cd = 0.508 + 1/(22.503 + 4.196*(v/w)^(2.5))^(0.4)*alpha*v;
Cm = etha*w/(2.022*w + 0.981*v)*alpha*v;

xdot = [ x(3)
         x(4)
         -Cd*x(3) + Cm*x(4)
         -g-Cd*x(4) - Cm*x(3) ]; 
endfunction
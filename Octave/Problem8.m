format long
y = @(x)exp(x/5);
x = [0:.133108:5];
p3 = polyfit(x,y(x),3);

%x=0.01i for i=[1,19]
range=[0.01:0.01:.19];
g=@(i)polyval(p3,i);
sol=arrayfun(@(i)polyval(p3,i),range);
actual=arrayfun(y,range);
table=[sol;abs(actual-sol)]'

%x=5-0.01i for i=[1,19]
%range=5-[0.01:0.01:.19];
%g=@(i)polyval(p3,i);
%sol=arrayfun(@(i)polyval(p3,i),range);
%actual=arrayfun(y,range);
%table2=[sol;abs(actual-sol)]'

plot(x,y(x),range,sol)
legend('show')
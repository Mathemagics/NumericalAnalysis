function dvdt = ode2(t,v);
% ODE2A: computes dvdt.
m =1; g = 9.81; k = 2;
dvdt = g-(k/m)*sqrt(abs(v));

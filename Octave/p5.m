function dzdt = dzdt(t,z); 
dzdt = [z(2);-z(1)/(z(1)^2+z(3)^2)^(3/2);z(4);-z(3)/(z(1)^2+z(3)^2)^(3/2)];
# Purpose:

 This is a final project for a numerical analysis course. The class consisted of about 5 students. Each student was handed a different set of questions by the professor, to be solved in matlab. Unfortunately, the quality of this document does not look very good when scanned, so I have omitted it. If I can find the original document, I plan on rewriting it. In any case, the solution document should contain enough information to be able to infer the type of problems asked.

# Contents: 
The pdf file contains the solutions and reasoning for each problem.  The Octave directory contains the programs used to obtain solutions. They were written for GNU/Octave, but should be compatible with matlab. There also seems to be WxMaxima files that I used to find algebraic solutions for some messy calculations.
